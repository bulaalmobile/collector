import 'dart:convert';
import 'dart:math';

import 'package:collector/Screens/dashboard.dart';
import 'package:collector/widgets/pass_field.dart';
import 'package:collector/widgets/user_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../network_handler.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool vis = true;

  final _globalkey = GlobalKey<FormState>();

  NetworkHandler networkHandler = NetworkHandler();

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  late String errorText;

  bool validate = false;

  bool circular = false;

  final storage = const FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Form(
              key: _globalkey,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 10.0),
                child: Column(
                  children: [
                    UsernameTextField(),
                    const SizedBox(
                      height: 10,
                    ),
                    const PasswordTextField(),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            InkWell(
              onTap: () async {
                setState(() {
                  circular = true;
                });

                //Login Logic start here
                Map<String, String> data = {
                  "user_name": _usernameController.text,
                  "password": _passwordController.text,
                };
                var response = await networkHandler.auth("/mobile/token", data);

                if (response.statusCode == 200 || response.statusCode == 201) {
                  Map<String, dynamic> output = json.decode(response.body);
                  log(output["token"]);
                  await storage.write(key: "token", value: output["token"]);
                  setState(() {
                    validate = true;
                    circular = false;
                  });
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const Dashboard(),
                      ),
                      (route) => false);
                } else {
                  String output = json.decode(response.body);
                  setState(() {
                    validate = false;
                    errorText = output;
                    circular = false;
                  });
                }

                // login logic End here
              },
              child: Container(
                width: 150,
                height: 50,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: const Color(0xff00A86B),
                ),
                child: Center(
                  child: circular
                      ? const CircularProgressIndicator()
                      : const Text(
                          "Sign In",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
