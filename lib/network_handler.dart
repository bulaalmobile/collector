import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';

class NetworkHandler {
  // Api URL
  String baseurl = "http://192.168.43.92:5000";
  // Fornarter
  String formater(String url) {
    return baseurl + url;
  }

  // Logger
  var log = Logger();
  // store data in secure storage
  FlutterSecureStorage storage = const FlutterSecureStorage();
  // Auth
  Future auth(String url, Map<String, String> data) async {
    String? token = await storage.read(key: "token");
    url = formater(url);
    var request = await http.get(
      Uri.parse(url),
      headers: {
        "Content-type": "application/json",
      },
    );

    if (request.statusCode == 200) {
      return json.decode(request.body);
    }
    log.i(request.body);
    log.i(request.statusCode);
    log.i(token);
  }

  // GET
  Future get(String url, Map<String, String> data) async {
    String? token = await storage.read(key: "token");
    url = formater(url);
    // /user/register
    var response = await http.get(
      Uri.parse(url),
      headers: {"Authorization": "Bearer $token"},
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      log.i(response.body);

      return json.decode(response.body);
    }
    log.i(response.body);
    log.i(response.statusCode);
  }

  // POST
  Future<http.Response> post(String url, Map<String, String> body) async {
    String? token = await storage.read(key: "token");
    url = formater(url);
    // log.d(body);
    log.d(token);
    var response = await http.post(
      Uri.parse(url),
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer $token"
      },
      body: json.encode(body),
    );
    return response;
  }
}
