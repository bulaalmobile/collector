import 'package:flutter/material.dart';

// ignore: must_be_immutable
class UsernameTextField extends StatelessWidget {
  bool vis = true;
  final TextEditingController _usernameController = TextEditingController();
  late String errorText;
  bool validate = false;
  bool circular = false;

  UsernameTextField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // const Text("Username"),
        TextFormField(
          controller: _usernameController,
          decoration: const InputDecoration(
            hintText: 'Username',
            // errorText: validate ? null : errorText,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.black,
                width: 2,
              ),
            ),
          ),
        )
      ],
    );
  }
}
