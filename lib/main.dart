import 'package:flutter/material.dart';

import 'Screens/login.dart';

void main() {
  runApp(const Collector());
}

class Collector extends StatelessWidget {
  const Collector({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
    );
  }
}
